import codecs
from PIL import Image
from itertools import islice
blackwhite_threshhold = 128*3


def get_pixels_as_list(filename):
	result = []
	img = Image.open(filename)
	width = img.size[0]
	height = img.size[1]
	for x in range(0, height):
		for y in range(0, width):
			color = img.getpixel((y,x))
			decider = color[0] + color[1] + color[2]
			if decider < blackwhite_threshhold:
				result.append(1)
			else:
				result.append(0)
	result.append({'width': width, 'height': height, 'color': color})
	return result

def process_glyph_descriptor(filename):
	def to_num_key(skey):
		test_val = skey.strip('"')
		test_val = test_val.strip("'")
		if test_val.isnumeric():
			return {'int': int(test_val)}

	def pack_glyph_array(glyph):
		packed = []
		width = glyph[-1]['width']
		height = glyph[-1]['height']
		for line in range(0, height):
			packed_line = 0
			for bit in range(0, width):
				index = line * width + bit
				value = glyph[index]
				bit_mask = value << bit
				packed_line = packed_line | bit_mask
			packed.append(packed_line)
		packed.append(glyph[-1])
		return packed

	def list_as_lua_table_def(pplist, key, height, width):
		if key == '"\\"':
			key = '"\\\\"'
		if key == "'":
			key = '"\'"'
		lua_table_def='[{0}]={{'.format(key)
		for value in pplist[:-1]:
			lua_table_def = lua_table_def + str(value) +','
		if pplist[-1]['width'] == width and pplist[-1]['height'] == height:
			lua_table_def = lua_table_def[:-1]
		else:
			if pplist[-1]['width'] != width:
				lua_table_def = lua_table_def + '["{}"]={}'.format('width', pplist[-1]['width'])
			if pplist[-1]['height'] != height:
				lua_table_def = lua_table_def + ',["{}"]={}'.format('height', pplist[-1]['height'])
		lua_table_def = lua_table_def + '}'
		return lua_table_def

	def output_element(key, element, separator, height, width):
		int_key = to_num_key(key)
		if int_key:
			int_key = int_key['int']
			return list_as_lua_table_def(element, int_key, height, width) + separator
		else:
			return list_as_lua_table_def(element, key, height, width) + separator

	def process_lines(f_pattern, iterator, separator, height, width):
		elem = ''
		for line in iterator:
			if elem:
				print(elem)
			line = line.rstrip()
			parameters = line.split(" ")
			char_filename = f_pattern.format(parameters[0])
			char_key = parameters[1]
			pixel_list = get_pixels_as_list(char_filename)
			packed_pixel_list = pack_glyph_array(pixel_list)
			elem = output_element(char_key, packed_pixel_list, separator, height, width)
		return elem



	list_file = codecs.open(filename,"r","utf-8")

	iter_file = islice(list_file,0,None)
	h = iter_file.__next__().rstrip()

	font_height = int(h)
	w = iter_file.__next__().rstrip()
	font_width = int(w)

	file_pattern = iter_file.__next__().rstrip()

	print('return {{\nheight="{}",\nwidth={},\ndepth=1,'.format(font_height, font_width))

	last_line = process_lines(file_pattern, iter_file, ',', font_height, font_width)
	print(last_line[:-1])
	print('}')


process_glyph_descriptor('descriptor.txt')